package com.suriya.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val log = "MAIN_ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edtName = findViewById<EditText>(R.id.edt_name)
        val btnHello = findViewById<Button>(R.id.btn_hello)
        val txtHello = findViewById<TextView>(R.id.txt_hello)
//        btn_hello.setOnClickListener(this)
//        btn_hello.setOnClickListener(object :View.OnClickListener{
//            override fun onClick(view: View?) {
//                Toast.makeText(this@MainActivity,"Click me",Toast.LENGTH_LONG).show()
//            }
//
//        })
        btnHello.setOnClickListener {
            Toast.makeText(this@MainActivity,"Hello : ${edtName.text.toString()}",Toast.LENGTH_LONG).show()
            Log.d(log,"Click me")
            txtHello.text = "Hello : ${edtName.text.toString()}"
        }
    }

    override fun onClick(view: View?) {
        Toast.makeText(this,"Click me",Toast.LENGTH_LONG).show()
    }
}